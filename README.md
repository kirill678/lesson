#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool delHelp(int a) {
    return a % 2 == 0;
}

vector<int> in() {
    int b = 1;
    vector<int> a;
    while (b != 0) {
        cin >> b;
        a.push_back(b);
    }
    return a;
}

void print(const vector<int>& a) {
    for (int i : a) {
        cout << i << endl;
    }
    cout << "" << endl;
}

int main() {
    vector<int> a = in();
    print(a);
    a.erase(remove(a.begin(), a.end(), 3), a.end());
    a.erase(remove_if(a.begin(), a.end(), delHelp), a.end());
    print(a);
    return 0;
}
